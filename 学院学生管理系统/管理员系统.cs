﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 学院学生管理系统
{
    public partial class 管理员系统 : Form
    {
        public 管理员系统()
        {
            InitializeComponent();
        }

        private void 重新登录ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void 记事本ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            记事本 q1 =new 记事本();
            q1.Show();
        }

        private void 关于记事本ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            关于记事本 q2 = new 关于记事本();
            q2.Show();
        }

        private void 操作说明ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            操作说明 a19 = new 操作说明();
            a19.Show();
        }

        private void 关于ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            关于 a20 = new 关于();
            a20.Show();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            添加用户 a1 = new 添加用户();
            a1.Show();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            修改密码 a2 = new 修改密码();
            a2.Show();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            查询或删除用户 a3 = new 查询或删除用户();
            a3.Show();
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            查询或删除用户 a4 = new 查询或删除用户();
            a4.Show();
        }

        private void 退出系统ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            登录页面 c1 = new 登录页面();
            c1.Show();
        }

        private void 退出系统ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("您确定退出吗?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void 管理员系统_Load(object sender, EventArgs e)
        {
            
            label1.Text = "时间：" + DateTime.Now.ToLongTimeString().ToString();
            timer1.Enabled = true;
            timer1.Interval = 100;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = "当前时间为：" + DateTime.Now.ToLongTimeString().ToString();
        }
    }
}
