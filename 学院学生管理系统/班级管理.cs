﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace 学院学生管理系统
{
    public partial class 班级管理 : Form
    {
        public 班级管理()
        {
            InitializeComponent();
        }

        private void 添加班级_Load(object sender, EventArgs e)
        {

        }

        private void tbbeizhu_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            string t;
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            t = "select * from 班级 where 班名='" + tbclass.Text.Trim() + "'";
            a2.CommandText = t;
            a2.Connection = a1;
            OleDbDataReader a3 = a2.ExecuteReader();
            if (a3.Read())
            {
                MessageBox.Show("信息已经存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                a3.Close();
                string y;
                y = "insert into 班级 values(" + "'" + tbclass.Text.Trim() + "','"+ textBox1.Text.Trim() +"','" + tbXiBu.Text.Trim() + "','" + tbrenshu.Text.Trim() + "','" + tbteacher.Text.Trim() + "','" + tbbeizhu.Text.Trim() + "')";
                if (tbclass.Text == "" ||textBox1.Text==""|| tbXiBu.Text == "" || tbrenshu.Text == "" || tbteacher.Text == "" || tbbeizhu.Text == "" )
                {
                    MessageBox.Show("信息不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    a2.CommandText = y;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("信息添加成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
            } OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
            OleDbCommand cmd = conn.CreateCommand();
            cmd.CommandText = "select * from 班级";
            conn.Open();
            OleDbDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            if (dr.HasRows)
            {
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    dt.Columns.Add(dr.GetName(i));
                }
                dt.Rows.Clear();
            }
            while (dr.Read())
            {
                DataRow row = dt.NewRow();
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    row[i] = dr[i];
                }
                dt.Rows.Add(row);
            }
            cmd.Dispose();
            conn.Close();
            dataGridView1.DataSource = dt;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void btXiuGai_Click(object sender, EventArgs e)
        {
            string t;
            t = "select * from 班级 where 班名='" + tbclass.Text.Trim() + "'";
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            if (tbclass.Text == "")
            {
                MessageBox.Show("请输入需要修改的班级的班名！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 班级 where 班名='" + tbclass.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    a3.Close();
                    string aw;
                    aw = "update 班级 set 年级='" + textBox1.Text.Trim() + "',系别='" + tbXiBu.Text.Trim() + "',人数='" + tbrenshu.Text.Trim() + "',班主任='" + tbteacher.Text.Trim() + "',备注='" + tbbeizhu.Text.Trim() + "'where 班名='" + tbclass.Text.Trim() + "'";
                    a2.CommandText = aw;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("修改信息成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
                    OleDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "select * from 班级";
                    conn.Open();
                    OleDbDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    if (dr.HasRows)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            dt.Columns.Add(dr.GetName(i));
                        }
                        dt.Rows.Clear();
                    }
                    while (dr.Read())
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            row[i] = dr[i];
                        }
                        dt.Rows.Add(row);
                    }
                    cmd.Dispose();
                    conn.Close(); 
                    dataGridView1.DataSource = dt;
                }
            }
        }

        private void tbteacher_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string t;
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            if (tbclass.Text == "")
            {
                MessageBox.Show("请输入班名！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 班级 where 班名='" + tbclass.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    tbclass.Text = a3[0].ToString();
                    textBox1.Text = a3[1].ToString();
                    tbXiBu.Text = a3[2].ToString();
                    tbrenshu.Text = a3[3].ToString();
                    tbteacher.Text = a3[4].ToString();
                    tbbeizhu.Text = a3[5].ToString();
                    MessageBox.Show("查询成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
                    OleDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "select * from 班级";
                    conn.Open();
                    OleDbDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    if (dr.HasRows)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            dt.Columns.Add(dr.GetName(i));
                        }
                        dt.Rows.Clear();
                    }
                    while (dr.Read())
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            row[i] = dr[i];
                        }
                        dt.Rows.Add(row);
                    }
                    cmd.Dispose();
                    conn.Close(); 
                    dataGridView1.DataSource = dt;
                }
            }
        }

        private void btQuert_Click(object sender, EventArgs e)
        {
            教师系统 c2 = new 教师系统();
            c2.Show();
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string t;
            t = "select * from 班级 where 班名='" + tbclass.Text.Trim() + "'";
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            if (tbclass.Text == "")
            {
                MessageBox.Show("请输入需要删除的班级的班名！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 班级 where 班名='" + tbclass.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    a3.Close();
                    string aw;
                    aw = "delete from 班级 where 班名='" + tbclass.Text.Trim() + "'";
                    a2.CommandText = aw;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("信息删除成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
                    OleDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "select * from 班级";
                    conn.Open();
                    OleDbDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    if (dr.HasRows)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            dt.Columns.Add(dr.GetName(i));
                        }
                        dt.Rows.Clear();
                    }
                    while (dr.Read())
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            row[i] = dr[i];
                        }
                        dt.Rows.Add(row);
                    }
                    cmd.Dispose();
                    conn.Close(); 
                    dataGridView1.DataSource = dt;
                }
            }
        }
    }
}
