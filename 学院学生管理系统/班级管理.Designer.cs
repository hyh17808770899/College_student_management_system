﻿namespace 学院学生管理系统
{
    partial class 班级管理
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btAdd = new System.Windows.Forms.Button();
            this.btQuert = new System.Windows.Forms.Button();
            this.btXiuGai = new System.Windows.Forms.Button();
            this.tbteacher = new System.Windows.Forms.TextBox();
            this.tbclass = new System.Windows.Forms.TextBox();
            this.tbrenshu = new System.Windows.Forms.TextBox();
            this.tbXiBu = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tbbeizhu = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(211, 508);
            this.btAdd.Margin = new System.Windows.Forms.Padding(4);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(100, 29);
            this.btAdd.TabIndex = 32;
            this.btAdd.Text = "添加";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // btQuert
            // 
            this.btQuert.Location = new System.Drawing.Point(536, 508);
            this.btQuert.Margin = new System.Windows.Forms.Padding(4);
            this.btQuert.Name = "btQuert";
            this.btQuert.Size = new System.Drawing.Size(100, 29);
            this.btQuert.TabIndex = 29;
            this.btQuert.Text = "退出";
            this.btQuert.UseVisualStyleBackColor = true;
            this.btQuert.Click += new System.EventHandler(this.btQuert_Click);
            // 
            // btXiuGai
            // 
            this.btXiuGai.Location = new System.Drawing.Point(319, 508);
            this.btXiuGai.Margin = new System.Windows.Forms.Padding(4);
            this.btXiuGai.Name = "btXiuGai";
            this.btXiuGai.Size = new System.Drawing.Size(100, 29);
            this.btXiuGai.TabIndex = 28;
            this.btXiuGai.Text = "修改";
            this.btXiuGai.UseVisualStyleBackColor = true;
            this.btXiuGai.Click += new System.EventHandler(this.btXiuGai_Click);
            // 
            // tbteacher
            // 
            this.tbteacher.Location = new System.Drawing.Point(529, 99);
            this.tbteacher.Margin = new System.Windows.Forms.Padding(4);
            this.tbteacher.Name = "tbteacher";
            this.tbteacher.Size = new System.Drawing.Size(132, 25);
            this.tbteacher.TabIndex = 26;
            this.tbteacher.TextChanged += new System.EventHandler(this.tbteacher_TextChanged);
            // 
            // tbclass
            // 
            this.tbclass.Location = new System.Drawing.Point(143, 48);
            this.tbclass.Margin = new System.Windows.Forms.Padding(4);
            this.tbclass.Name = "tbclass";
            this.tbclass.Size = new System.Drawing.Size(132, 25);
            this.tbclass.TabIndex = 25;
            // 
            // tbrenshu
            // 
            this.tbrenshu.Location = new System.Drawing.Point(143, 99);
            this.tbrenshu.Margin = new System.Windows.Forms.Padding(4);
            this.tbrenshu.Name = "tbrenshu";
            this.tbrenshu.Size = new System.Drawing.Size(132, 25);
            this.tbrenshu.TabIndex = 24;
            // 
            // tbXiBu
            // 
            this.tbXiBu.Location = new System.Drawing.Point(529, 48);
            this.tbXiBu.Margin = new System.Windows.Forms.Padding(4);
            this.tbXiBu.Name = "tbXiBu";
            this.tbXiBu.Size = new System.Drawing.Size(132, 25);
            this.tbXiBu.TabIndex = 23;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(63, 161);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 15);
            this.label5.TabIndex = 22;
            this.label5.Text = "备注";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(455, 102);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 21;
            this.label4.Text = "班主任";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 51);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 15);
            this.label3.TabIndex = 20;
            this.label3.Text = "班名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 102);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 15);
            this.label2.TabIndex = 19;
            this.label2.Text = "人数";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(470, 51);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 15);
            this.label1.TabIndex = 18;
            this.label1.Text = "系别";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(99, 508);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 29);
            this.button1.TabIndex = 33;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(65, 288);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(597, 188);
            this.dataGridView1.TabIndex = 30;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // tbbeizhu
            // 
            this.tbbeizhu.HideSelection = false;
            this.tbbeizhu.Location = new System.Drawing.Point(143, 158);
            this.tbbeizhu.Margin = new System.Windows.Forms.Padding(4);
            this.tbbeizhu.Multiline = true;
            this.tbbeizhu.Name = "tbbeizhu";
            this.tbbeizhu.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbbeizhu.Size = new System.Drawing.Size(519, 96);
            this.tbbeizhu.TabIndex = 27;
            this.tbbeizhu.TextChanged += new System.EventHandler(this.tbbeizhu_TextChanged);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(428, 508);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 29);
            this.button2.TabIndex = 34;
            this.button2.Text = "删除";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(344, 51);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(75, 25);
            this.textBox1.TabIndex = 36;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(299, 51);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 15);
            this.label6.TabIndex = 35;
            this.label6.Text = "年级";
            // 
            // 班级管理
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Beige;
            this.ClientSize = new System.Drawing.Size(735, 576);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btAdd);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btQuert);
            this.Controls.Add(this.btXiuGai);
            this.Controls.Add(this.tbbeizhu);
            this.Controls.Add(this.tbteacher);
            this.Controls.Add(this.tbclass);
            this.Controls.Add(this.tbrenshu);
            this.Controls.Add(this.tbXiBu);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "班级管理";
            this.Text = "班级管理";
            this.Load += new System.EventHandler(this.添加班级_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Button btQuert;
        private System.Windows.Forms.Button btXiuGai;
        private System.Windows.Forms.TextBox tbteacher;
        private System.Windows.Forms.TextBox tbclass;
        private System.Windows.Forms.TextBox tbrenshu;
        private System.Windows.Forms.TextBox tbXiBu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox tbbeizhu;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
    }
}