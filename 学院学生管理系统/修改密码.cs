﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;


namespace 学院学生管理系统
{
    public partial class 修改密码 : Form
    {
        public 修改密码()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string t;
            t = "select * from 登录信息 where 账号='" + tbname.Text.Trim() + "'and 密码='"+oldPwd.Text.Trim()+"'";
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();
            string w; 
            w = "select * from 登录信息 where 帐号='" + tbname.Text.Trim() + "'and 密码='" + oldPwd.Text.Trim() + "'";
            OleDbConnection a4 = new OleDbConnection();
            OleDbCommand a5 = new OleDbCommand();
            a4.ConnectionString = Class1.mas;
            if (tbname.Text == "")
            {
                MessageBox.Show("请输入需要修改的账号！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else 
            {
                t = "select * from 登录信息 where 帐号='" + tbname.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (oldPwd.Text.ToString().Trim() == "")
                {
                    MessageBox.Show("请输入旧密码！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (tbNewpwd.Text.ToString().Trim() == "")
                {
                    MessageBox.Show("请输入新密码！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (oldPwd.Text.ToString().Trim() == tbNewpwd.Text.ToString().Trim())
                {
                    MessageBox.Show("新密码和旧密码相同，请重新输入！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (repwd.Text.ToString().Trim() == "")
                {
                    MessageBox.Show("请确认密码！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (tbNewpwd.Text.ToString().Trim() != repwd.Text.ToString().Trim())
                {
                    MessageBox.Show("确认密码与新密码不一致！");
                    repwd.Text = "";
                    repwd.Focus();
                }
                else 
                {
                    a3.Close();
                    
                    a4.Open(); 
                    w = "select * from 登录信息 where 帐号='" + tbname.Text.Trim() + "'and 密码='" + oldPwd.Text.Trim() + "'";
                    a5.CommandText = w;
                    a5.Connection = a4;
                    OleDbDataReader a6=a5.ExecuteReader();
                    if (a6.Read())
                    {
                        a6.Close();
                        string aw;
                        aw = "update 登录信息 set 密码='" + repwd.Text.Trim() + "'where 帐号='" + tbname.Text.Trim() + "'";
                        a2.CommandText = aw;
                        a2.Connection = a1;
                        a2.ExecuteNonQuery();
                        MessageBox.Show("密码修改成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        MessageBox.Show("旧密码输入错误！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void oldPwd_TextChanged(object sender, EventArgs e)
        {

        }

        private void tbNewpwd_TextChanged(object sender, EventArgs e)
        {

        }

        private void repwd_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
            
        }
    }

