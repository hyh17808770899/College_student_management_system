﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace 学院学生管理系统
{
    public partial class 添加用户 : Form
    {
        public 添加用户()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("用户名或密码不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                string wa;
                OleDbConnection a1 = new OleDbConnection();
                OleDbCommand a2 = new OleDbCommand();
                a1.ConnectionString = Class1.mas;
                a1.Open();
                wa = "Select * from 登录信息 where 帐号='" + textBox1.Text.Trim() + "'";
                a2.CommandText = wa;
                a2.Connection = a1;
                a2.ExecuteScalar();
                if (null != a2.ExecuteScalar())
                {
                    MessageBox.Show("该帐号已经存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (textBox3.Text == "")
                {
                    MessageBox.Show("请确认密码！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (textBox3.Text != textBox2.Text)
                {
                    MessageBox.Show("确认密码错误！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (comboBox1.Text == "")
                {
                    MessageBox.Show("请选择用户类型！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (textBox3.Text == textBox2.Text)
                {
                    string wb;
                    wb = "INSERT into 登录信息 values(" + "'" + textBox1.Text.Trim() + "','" + textBox2.Text.Trim() + "','" + comboBox1.Text.Trim() + "')";
                    a2.CommandText = wb;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("用户添加成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void btQueit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
