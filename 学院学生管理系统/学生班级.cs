﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace 学院学生管理系统
{
    public partial class 学生班级 : Form
    {
        public 学生班级()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string t;
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            if (tbclass.Text == "")
            {
                MessageBox.Show("请输入班名！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 班级 where 班名='" + tbclass.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    tbclass.Text = a3[0].ToString();
                    textBox1.Text = a3[1].ToString();
                    tbXiBu.Text = a3[2].ToString();
                    tbrenshu.Text = a3[3].ToString();
                    tbteacher.Text = a3[4].ToString();
                    tbbeizhu.Text = a3[5].ToString();
                    MessageBox.Show("查询成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
                    OleDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "select * from 班级";
                    conn.Open();
                    OleDbDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    if (dr.HasRows)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            dt.Columns.Add(dr.GetName(i));
                        }
                        dt.Rows.Clear();
                    }
                    while (dr.Read())
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            row[i] = dr[i];
                        }
                        dt.Rows.Add(row);
                    }
                    cmd.Dispose();
                    conn.Close();
                    dataGridView1.DataSource = dt;
                }
            }
        }
    }
}
