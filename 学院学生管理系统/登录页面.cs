﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.IO;
using System.Drawing.Drawing2D;

namespace 学院学生管理系统
{
    public partial class 登录页面 : Form
    {
        string verifyCode = string.Empty;
        private void RefrashVerifyCodeSimple()
        {
            VerifyCodeImgSimple code = new VerifyCodeImgSimple();
            verifyCode = code.CreateVerifyCode(4);
            pictureBox1.Image = code.CreateImage(verifyCode, 35, "楷体", Color.Blue, Color.White);
        }
        public 登录页面()
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            textBox2.PasswordChar = '*';
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void 登录页面_Load(object sender, EventArgs e)
        {
            RefrashVerifyCodeSimple();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            RefrashVerifyCodeSimple();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            label5.Text = "";
            if (radioButton1.Checked) label5.Text += radioButton1.Text;
            else if (radioButton2.Checked) label5.Text += radioButton2.Text;
            else if (radioButton3.Checked) label5.Text += radioButton3.Text;
                string w;
                OleDbConnection a1 = new OleDbConnection();
                OleDbCommand a2 = new OleDbCommand();
                a1.ConnectionString = Class1.mas;
                a1.Open();
                w = "select * from 登录信息 where 帐号='" + textBox1.Text.Trim() + "'and 密码='" + textBox2.Text.Trim() + "'and 用户身份='" + label5.Text.Trim() + "'";
                a2.CommandText = w;
                a2.Connection = a1;
                a2.ExecuteScalar();
                if (null != a2.ExecuteScalar() && verifyCode.ToUpper() == this.textBox3.Text.ToUpper())
                {
                    MessageBox.Show("登陆成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    if (label5.Text == "教师")
                    {
                        教师系统 f3 = new 教师系统();
                        f3.Show();
                    }
                    else if (label5.Text == "学生")
                    {
                        学生系统 f4 = new 学生系统();
                        f4.Show();
                    }
                    else if (label5.Text == "管理员")
                    {
                        管理员系统 f5 = new 管理员系统();
                        f5.Show();
                    }
                    this.Hide();
                }
                else if (textBox1.Text == "" || textBox2.Text == "")
                {
                    MessageBox.Show("请输入账号或密码！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if(textBox3.Text == "")
                {
                    MessageBox.Show("请输入验证码！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (verifyCode.ToUpper() != this.textBox3.Text.ToUpper())
                {
                    MessageBox.Show("验证码输入有误/n  请重新输入", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.textBox3.Text = "";
                    RefrashVerifyCodeSimple();
                }
                else if (label5.Text == "")
                {
                    MessageBox.Show("请选择用户身份！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    MessageBox.Show("    登陆失败\n账号或密码错误！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            注册页面 f1 = new 注册页面();
            f1.Show();
            this.Hide();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
