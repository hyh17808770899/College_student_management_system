﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class 宿舍管理系统 : Form
    {
        public 宿舍管理系统()
        {
            InitializeComponent();
        }


      

        private void 出入登记ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            出入登记 f4 = new 出入登记();
            f4.ShowDialog();
        }

        private void 人员来访登记ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            来访登记 f5 = new 来访登记();
            f5.ShowDialog();
        }

        private void 故障报修ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            宿舍故障报修 f6 = new 宿舍故障报修();
            f6.ShowDialog();
        }

        private void 电费充值ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            电费充值 f6 = new 电费充值();
            f6.ShowDialog();
        }

        private void 水费充值ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            水费充值 f1 = new 水费充值();
            f1.ShowDialog();
        }

        private void 住宿费缴费ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            住宿费缴费 f1 = new 住宿费缴费();
            f1.ShowDialog();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            出入登记查询 f1 =  new 出入登记查询();
            f1.ShowDialog();
        }

        private void 人员来访登记查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            人员来访登记查询 f1 = new 人员来访登记查询();
            f1.ShowDialog();
        }

        private void 故障保修查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            宿舍故障报修查询 f7 = new 宿舍故障报修查询();
            f7.ShowDialog();
            
        }
    }
}
