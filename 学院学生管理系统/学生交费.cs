﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace 学院学生管理系统
{
    public partial class 学生交费 : Form
    {
        public 学生交费()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string t;
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            t = "select * from 学生交费 where 学号='" + textBox1.Text.Trim() + "'";
            a2.CommandText = t;
            a2.Connection = a1;
            OleDbDataReader a3 = a2.ExecuteReader();
            if (a3.Read())
            {
                MessageBox.Show("信息已经存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                a3.Close();
                string y;
                
                if (textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" ||pictureBox1.ImageLocation=="")
                {
                    MessageBox.Show("信息不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    y = "insert into 学生交费 values(" + "'" + textBox1.Text.Trim() + "','" + textBox2.Text.Trim() + "','" + textBox3.Text.Trim() + "','" + textBox4.Text.Trim() + "','" + Program.Imagefile + "')";
                    a2.CommandText = y;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("信息添加成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dr = new OpenFileDialog();
            dr.ShowDialog();
            dr.Filter = "image file(*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF";
            string ImageFile = dr.FileName;
            pictureBox1.ImageLocation = ImageFile;
            Program.Imagefile = ImageFile;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            教师系统 a3 = new 教师系统();
            a3.Show();
            this.Hide();
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            string s;
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();
            if (textBox1.Text == "")
            {
                MessageBox.Show("请输入需要查询学生的学号！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                s = "select * from 学生交费 where 学号='" + textBox1.Text.ToString().Trim() + "'";
                a2.CommandText = s;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("此学号尚未注册！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    textBox1.Text = a3[0].ToString();
                    textBox2.Text = a3[1].ToString();
                    textBox3.Text = a3[2].ToString();
                    textBox4.Text = a3[3].ToString();
                    pictureBox1.ImageLocation = a3[4].ToString();
                    MessageBox.Show("查询成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }  
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string t;
            t = "select * from 学生交费 where 学号='" + textBox1.Text.Trim() + "'";
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            if (textBox1.Text == "")
            {
                MessageBox.Show("请输入需要修改的学生的学号！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 学生交费 where 学号='" + textBox1.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    a3.Close();
                    string aw;
                    aw = "update 学生交费 set 姓名='" + textBox2.Text.Trim() + "',班级='" + textBox3.Text.Trim() + "',银行卡卡号='" + textBox4.Text.Trim() + "',照片='" + Program.Imagefile + "'";
                    a2.CommandText = aw;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("修改信息成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void 学生交费_Load(object sender, EventArgs e)
        {
            
        }
    }
}
