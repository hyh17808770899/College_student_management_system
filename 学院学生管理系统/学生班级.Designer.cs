﻿namespace 学院学生管理系统
{
    partial class 学生班级
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tbbeizhu = new System.Windows.Forms.TextBox();
            this.tbteacher = new System.Windows.Forms.TextBox();
            this.tbclass = new System.Windows.Forms.TextBox();
            this.tbrenshu = new System.Windows.Forms.TextBox();
            this.tbXiBu = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(325, 57);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(75, 25);
            this.textBox1.TabIndex = 49;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(280, 57);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 15);
            this.label6.TabIndex = 48;
            this.label6.Text = "年级";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(46, 294);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.Size = new System.Drawing.Size(597, 188);
            this.dataGridView1.TabIndex = 47;
            // 
            // tbbeizhu
            // 
            this.tbbeizhu.HideSelection = false;
            this.tbbeizhu.Location = new System.Drawing.Point(124, 164);
            this.tbbeizhu.Margin = new System.Windows.Forms.Padding(4);
            this.tbbeizhu.Multiline = true;
            this.tbbeizhu.Name = "tbbeizhu";
            this.tbbeizhu.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbbeizhu.Size = new System.Drawing.Size(519, 96);
            this.tbbeizhu.TabIndex = 46;
            // 
            // tbteacher
            // 
            this.tbteacher.Location = new System.Drawing.Point(510, 105);
            this.tbteacher.Margin = new System.Windows.Forms.Padding(4);
            this.tbteacher.Name = "tbteacher";
            this.tbteacher.Size = new System.Drawing.Size(132, 25);
            this.tbteacher.TabIndex = 45;
            // 
            // tbclass
            // 
            this.tbclass.Location = new System.Drawing.Point(124, 54);
            this.tbclass.Margin = new System.Windows.Forms.Padding(4);
            this.tbclass.Name = "tbclass";
            this.tbclass.Size = new System.Drawing.Size(132, 25);
            this.tbclass.TabIndex = 44;
            // 
            // tbrenshu
            // 
            this.tbrenshu.Location = new System.Drawing.Point(124, 105);
            this.tbrenshu.Margin = new System.Windows.Forms.Padding(4);
            this.tbrenshu.Name = "tbrenshu";
            this.tbrenshu.Size = new System.Drawing.Size(132, 25);
            this.tbrenshu.TabIndex = 43;
            // 
            // tbXiBu
            // 
            this.tbXiBu.Location = new System.Drawing.Point(510, 54);
            this.tbXiBu.Margin = new System.Windows.Forms.Padding(4);
            this.tbXiBu.Name = "tbXiBu";
            this.tbXiBu.Size = new System.Drawing.Size(132, 25);
            this.tbXiBu.TabIndex = 42;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 167);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 15);
            this.label5.TabIndex = 41;
            this.label5.Text = "备注";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(436, 108);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 40;
            this.label4.Text = "班主任";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 57);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 15);
            this.label3.TabIndex = 39;
            this.label3.Text = "班名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 108);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 15);
            this.label2.TabIndex = 38;
            this.label2.Text = "人数";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(451, 57);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 15);
            this.label1.TabIndex = 37;
            this.label1.Text = "系别";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(283, 490);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 29);
            this.button1.TabIndex = 50;
            this.button1.Text = "查询";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // 学生班级
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(698, 566);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.tbbeizhu);
            this.Controls.Add(this.tbteacher);
            this.Controls.Add(this.tbclass);
            this.Controls.Add(this.tbrenshu);
            this.Controls.Add(this.tbXiBu);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "学生班级";
            this.Text = "学生班级";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.TextBox tbbeizhu;
        private System.Windows.Forms.TextBox tbteacher;
        private System.Windows.Forms.TextBox tbclass;
        private System.Windows.Forms.TextBox tbrenshu;
        private System.Windows.Forms.TextBox tbXiBu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
    }
}