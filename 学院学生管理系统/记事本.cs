﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 学院学生管理系统
{
    public partial class 记事本 : Form
    {
        private OpenFileDialog od = new OpenFileDialog();        
        private SaveFileDialog sd = new SaveFileDialog();        
        private bool richboxTextHasChanged = false;

        public 记事本()
        {
            InitializeComponent();
        }

        private void 帮助HToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://answers.microsoft.com/en-us/windows/forum/apps_windows_10");
        }

        private void 记事本_Load(object sender, EventArgs e)
        {
            od.FileName = "";            
            od.Filter = "TXT FILE(*.txt)|*.txt";            
            sd.Filter = "TXT FILE(*.txt)|*.txt";            
            label2.Text = "时间：" + DateTime.Now.ToLongTimeString().ToString();            
            timer1.Enabled = true;            
            timer1.Interval = 100;
        }

        private void 新建NToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (od.FileName != "" && richboxTextHasChanged == true && MessageBox.Show("文本内容已更改\n是否保存修改？", "信息提示", MessageBoxButtons.OKCancel) == System.Windows.Forms.DialogResult.OK)

            {//若文本改动，首先保存

                richTextBox1.SaveFile(od.FileName, RichTextBoxStreamType.PlainText);

            }

            //不然初始化界面

            od.FileName = "";

            this.Text = "记事本";

            this.richTextBox1.Clear();

            this.richboxTextHasChanged = false;
        }

        private void 打开OToolStripMenuItem_Click(object sender, EventArgs e)
        {
            od.ShowDialog();//选择打开文本文件            
            if (od.FileName != "")
            {
                //将文本文件里面的内容加载到界面内                
                richTextBox1.LoadFile(od.FileName, RichTextBoxStreamType.PlainText);
                this.Text = od.FileName + "-记事本";
            }
        }

        private void 保存SToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (od.FileName != "")
            {
                richTextBox1.SaveFile(od.FileName, RichTextBoxStreamType.PlainText);
            }
            else
            {
                MessageBox.Show("请先打开文本文件", "信息提示", MessageBoxButtons.OK);
            }
        }

        private void 另存为AToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sd.ShowDialog() == System.Windows.Forms.DialogResult.OK)            
            {                
                richTextBox1.SaveFile(sd.FileName, RichTextBoxStreamType.PlainText);            
            }

        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Dispose();

            this.Close();
        }

        private void 剪切TToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        private void 复制CToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        private void 粘贴PToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void 字体ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //设置字体

            FontDialog fd = new FontDialog();

            fd.ShowDialog();

            richTextBox1.Font = fd.Font;
        }

        private void toolStripMenuItem6_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();
        }

        private void 日期时间ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText(System.DateTime.Now.ToString());
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            richboxTextHasChanged = true;
        }
        private void richTextBox1_SelectionChanged(object sender, EventArgs e)
        {

            if (richTextBox1.SelectedText != "")//如果鼠标选中了文本内容
            {

                this.剪切ToolStripMenuItem.Enabled = true;

                this.复制ToolStripMenuItem.Enabled = true;

                this.剪切TToolStripMenuItem.Enabled = true;

                this.复制CToolStripMenuItem.Enabled = true;

            }

            else//如果鼠标没有选中文本内容
            {

                this.剪切ToolStripMenuItem.Enabled = false;

                this.复制ToolStripMenuItem.Enabled = false;

                this.剪切TToolStripMenuItem.Enabled = false;

                this.复制CToolStripMenuItem.Enabled = false;

            }

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            richTextBox1.Cut();
        }

        //鼠标点击的时候触发

        private void richTextBox1_MouseDown(object sender, MouseEventArgs e)
        {

            int index = richTextBox1.GetFirstCharIndexOfCurrentLine();

            int line = richTextBox1.GetLineFromCharIndex(index) + 1;

            int col = richTextBox1.SelectionStart - index + 1;

            label1.Text = "第 " + line.ToString() + " 行，第 " + col.ToString() + " 列";

        }

        //首次按下某个键的时候触发

        private void richTextBox1_KeyDown(object sender, KeyEventArgs e)
        {

            int index = richTextBox1.GetFirstCharIndexOfCurrentLine();

            int line = richTextBox1.GetLineFromCharIndex(index) + 1;

            int col = richTextBox1.SelectionStart - index + 1;

            label1.Text = "第 " + line.ToString() + " 行，第 " + col.ToString() + " 列";

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //没隔一段时间更新下时间信息

            label2.Text = "时间：" + DateTime.Now.ToLongTimeString().ToString();
        }

        private void 复制ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.Copy();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            richTextBox1.Paste();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            //设置字体

            FontDialog fd = new FontDialog();

            fd.ShowDialog();

            richTextBox1.Font = fd.Font;
        }

        private void 全选ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            richTextBox1.SelectAll();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            richTextBox1.AppendText(System.DateTime.Now.ToString());
        }

        private void 关于ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            关于记事本 s1 = new 关于记事本();
            s1.Show();
        }

    }
}
