﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace 学院学生管理系统
{
    public partial class 成绩查询 : Form
    {
        public 成绩查询()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string s;
            s = "select * from 成绩单 where 学号='" + textBox1.Text.ToString().Trim() + "'";
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();
            if (textBox1.Text == "")
            {
                MessageBox.Show("请输入需要查询学生的学号！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                a2.CommandText = s;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("此学号尚未注册！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
                    OleDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "select * from 成绩单 where 学号='" + textBox1.Text.ToString().Trim() + "'";
                    conn.Open();
                    OleDbDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    if (dr.HasRows)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            dt.Columns.Add(dr.GetName(i));
                        }
                        dt.Rows.Clear();
                    }
                    while (dr.Read())
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            row[i] = dr[i];
                        }
                        dt.Rows.Add(row);
                    }
                    cmd.Dispose();
                    conn.Close();
                    dataGridView1.DataSource = dt;
                    MessageBox.Show("查询成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }   
        }
    }
}
