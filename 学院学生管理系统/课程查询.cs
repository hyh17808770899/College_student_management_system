﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace 学院学生管理系统
{
    public partial class 课程查询 : Form
    {
        public 课程查询()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string mysql;
            OleDbConnection cn = new OleDbConnection();
            OleDbCommand mycmd = new OleDbCommand();
            cn.ConnectionString = Class1.mas;
            cn.Open();
            if (textBox1.Text == "")
                MessageBox.Show("请输入查询的教师姓名！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
            {
                mysql = "SELECT * FROM 课程表 WHERE 姓名 ='" + textBox1.Text.Trim() + "'";
                mycmd.CommandText = mysql;
                mycmd.Connection = cn;
                OleDbDataReader dr = mycmd.ExecuteReader();
                if (dr.Read() == false)
                {
                    MessageBox.Show("无该教师课程信息！", "友情提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                else
                {
                    label2.Text = dr[0].ToString();
                    label3.Text = dr[1].ToString();
                    label4.Text = dr[2].ToString();
                    label5.Text = dr[3].ToString();
                    label6.Text = dr[4].ToString();
                    label7.Text = dr[5].ToString();
                    //some code

                    int index = this.dataGridView1.Rows.Add();
                    this.dataGridView1.Rows[index].Cells[0].Value = label2.Text;
                    this.dataGridView1.Rows[index].Cells[1].Value = label3.Text;
                    this.dataGridView1.Rows[index].Cells[2].Value = label4.Text;
                    this.dataGridView1.Rows[index].Cells[3].Value = label5.Text;
                    this.dataGridView1.Rows[index].Cells[4].Value = label6.Text;
                    this.dataGridView1.Rows[index].Cells[5].Value = label7.Text;

                    label2.Text = "";
                    label3.Text = "";
                    label4.Text = "";
                    label5.Text = "";
                    label6.Text = "";
                    label7.Text = "";


                }
            }
        }
    }
}
