﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace 学院学生管理系统
{
    public partial class 查询或删除用户 : Form
    {
        public 查询或删除用户()
        {
            InitializeComponent();
        }

        private void btDelete_Click(object sender, EventArgs e)
        {
            string t;
            t = "select * from 登录信息 where 帐号='" + textBox1.Text.Trim() + "'";
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            if (textBox1.Text == "")
            {
                MessageBox.Show("请输入需要删除的用户的账号！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 登录信息 where 帐号='" + textBox1.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    a3.Close();
                    string aw;
                    aw = "delete from 登录信息 where 帐号='" + textBox1.Text.Trim() + "'";
                    a2.CommandText = aw;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("信息删除成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    textBox1.Text = "";
                    textBox2.Text = "";
                    textBox3.Text = "";
                }
            
            }
        }

        private void 删除用户_Load(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string t;
            OleDbConnection a4 = new OleDbConnection();
            OleDbCommand a5 = new OleDbCommand();
            a4.ConnectionString = Class1.mas;
            a4.Open();
            if (textBox1.Text == "")
            {
                MessageBox.Show("请输入账号！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 登录信息 where 帐号='" + textBox1.Text.Trim() + "'";
                a5.CommandText = t;
                a5.Connection = a4;
                OleDbDataReader a6 = a5.ExecuteReader();
                if (a6.Read()==false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    textBox1.Text = a6[0].ToString();
                    textBox2.Text = a6[1].ToString();
                    textBox3.Text = a6[2].ToString();
                    MessageBox.Show("查询成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void tbQuert_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
