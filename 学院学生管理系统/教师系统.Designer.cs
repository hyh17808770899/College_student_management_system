﻿namespace 学院学生管理系统
{
    partial class 教师系统
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.系统设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.增加用户ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.修改密码ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除用户ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.查询用户ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.重新登录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出系统ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出系统ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.学籍管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.档案增加ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.档案查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.注销学生管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.档案删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.班级管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.添加班级ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.班级查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除班级ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.课程管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.课程添加ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.课程查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.课程修改ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.成绩管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.成绩添加ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.成绩查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.修改成绩ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除成绩ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.缴费管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.学生交费增加ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.学生交费查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.学生交费修改ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.宿舍管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.操作说明ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于记事本ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.刷新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出系统ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.系统设置ToolStripMenuItem,
            this.学籍管理ToolStripMenuItem,
            this.班级管理ToolStripMenuItem,
            this.课程管理ToolStripMenuItem,
            this.成绩管理ToolStripMenuItem,
            this.缴费管理ToolStripMenuItem,
            this.toolStripMenuItem1,
            this.帮助ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(872, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStrip1_ItemClicked);
            // 
            // 系统设置ToolStripMenuItem
            // 
            this.系统设置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.用户管理ToolStripMenuItem,
            this.重新登录ToolStripMenuItem,
            this.退出系统ToolStripMenuItem,
            this.退出系统ToolStripMenuItem2});
            this.系统设置ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.系统设置ToolStripMenuItem.Name = "系统设置ToolStripMenuItem";
            this.系统设置ToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.系统设置ToolStripMenuItem.Text = "系统设置";
            // 
            // 用户管理ToolStripMenuItem
            // 
            this.用户管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.增加用户ToolStripMenuItem,
            this.修改密码ToolStripMenuItem,
            this.删除用户ToolStripMenuItem,
            this.查询用户ToolStripMenuItem});
            this.用户管理ToolStripMenuItem.Name = "用户管理ToolStripMenuItem";
            this.用户管理ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.用户管理ToolStripMenuItem.Text = "用户管理";
            this.用户管理ToolStripMenuItem.Click += new System.EventHandler(this.用户管理ToolStripMenuItem_Click);
            // 
            // 增加用户ToolStripMenuItem
            // 
            this.增加用户ToolStripMenuItem.Name = "增加用户ToolStripMenuItem";
            this.增加用户ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.增加用户ToolStripMenuItem.Text = "添加用户";
            this.增加用户ToolStripMenuItem.Click += new System.EventHandler(this.增加用户ToolStripMenuItem_Click);
            // 
            // 修改密码ToolStripMenuItem
            // 
            this.修改密码ToolStripMenuItem.Name = "修改密码ToolStripMenuItem";
            this.修改密码ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.修改密码ToolStripMenuItem.Text = "修改密码";
            this.修改密码ToolStripMenuItem.Click += new System.EventHandler(this.修改密码ToolStripMenuItem_Click);
            // 
            // 删除用户ToolStripMenuItem
            // 
            this.删除用户ToolStripMenuItem.Name = "删除用户ToolStripMenuItem";
            this.删除用户ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.删除用户ToolStripMenuItem.Text = "删除用户";
            this.删除用户ToolStripMenuItem.Click += new System.EventHandler(this.删除用户ToolStripMenuItem_Click);
            // 
            // 查询用户ToolStripMenuItem
            // 
            this.查询用户ToolStripMenuItem.Name = "查询用户ToolStripMenuItem";
            this.查询用户ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.查询用户ToolStripMenuItem.Text = "查询用户";
            this.查询用户ToolStripMenuItem.Click += new System.EventHandler(this.查询用户ToolStripMenuItem_Click);
            // 
            // 重新登录ToolStripMenuItem
            // 
            this.重新登录ToolStripMenuItem.Name = "重新登录ToolStripMenuItem";
            this.重新登录ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.重新登录ToolStripMenuItem.Text = "记事本";
            this.重新登录ToolStripMenuItem.Click += new System.EventHandler(this.重新登录ToolStripMenuItem_Click);
            // 
            // 退出系统ToolStripMenuItem
            // 
            this.退出系统ToolStripMenuItem.Name = "退出系统ToolStripMenuItem";
            this.退出系统ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.退出系统ToolStripMenuItem.Text = "重新登录";
            this.退出系统ToolStripMenuItem.Click += new System.EventHandler(this.退出系统ToolStripMenuItem_Click);
            // 
            // 退出系统ToolStripMenuItem2
            // 
            this.退出系统ToolStripMenuItem2.Name = "退出系统ToolStripMenuItem2";
            this.退出系统ToolStripMenuItem2.Size = new System.Drawing.Size(138, 24);
            this.退出系统ToolStripMenuItem2.Text = "退出系统";
            this.退出系统ToolStripMenuItem2.Click += new System.EventHandler(this.退出系统ToolStripMenuItem2_Click);
            // 
            // 学籍管理ToolStripMenuItem
            // 
            this.学籍管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.档案增加ToolStripMenuItem,
            this.档案查询ToolStripMenuItem,
            this.注销学生管理ToolStripMenuItem,
            this.档案删除ToolStripMenuItem});
            this.学籍管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.学籍管理ToolStripMenuItem.Name = "学籍管理ToolStripMenuItem";
            this.学籍管理ToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.学籍管理ToolStripMenuItem.Text = "学籍管理";
            // 
            // 档案增加ToolStripMenuItem
            // 
            this.档案增加ToolStripMenuItem.Name = "档案增加ToolStripMenuItem";
            this.档案增加ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.档案增加ToolStripMenuItem.Text = "添加档案";
            this.档案增加ToolStripMenuItem.Click += new System.EventHandler(this.档案增加ToolStripMenuItem_Click);
            // 
            // 档案查询ToolStripMenuItem
            // 
            this.档案查询ToolStripMenuItem.Name = "档案查询ToolStripMenuItem";
            this.档案查询ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.档案查询ToolStripMenuItem.Text = "档案查询";
            this.档案查询ToolStripMenuItem.Click += new System.EventHandler(this.档案查询ToolStripMenuItem_Click);
            // 
            // 注销学生管理ToolStripMenuItem
            // 
            this.注销学生管理ToolStripMenuItem.Name = "注销学生管理ToolStripMenuItem";
            this.注销学生管理ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.注销学生管理ToolStripMenuItem.Text = "档案修改";
            this.注销学生管理ToolStripMenuItem.Click += new System.EventHandler(this.档案修改ToolStripMenuItem_Click);
            // 
            // 档案删除ToolStripMenuItem
            // 
            this.档案删除ToolStripMenuItem.Name = "档案删除ToolStripMenuItem";
            this.档案删除ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.档案删除ToolStripMenuItem.Text = "档案删除";
            this.档案删除ToolStripMenuItem.Click += new System.EventHandler(this.档案删除ToolStripMenuItem_Click);
            // 
            // 班级管理ToolStripMenuItem
            // 
            this.班级管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.添加班级ToolStripMenuItem,
            this.班级查询ToolStripMenuItem,
            this.删除班级ToolStripMenuItem});
            this.班级管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.班级管理ToolStripMenuItem.Name = "班级管理ToolStripMenuItem";
            this.班级管理ToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.班级管理ToolStripMenuItem.Text = "班级管理";
            // 
            // 添加班级ToolStripMenuItem
            // 
            this.添加班级ToolStripMenuItem.Name = "添加班级ToolStripMenuItem";
            this.添加班级ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.添加班级ToolStripMenuItem.Text = "添加班级";
            this.添加班级ToolStripMenuItem.Click += new System.EventHandler(this.添加班级ToolStripMenuItem_Click);
            // 
            // 班级查询ToolStripMenuItem
            // 
            this.班级查询ToolStripMenuItem.Name = "班级查询ToolStripMenuItem";
            this.班级查询ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.班级查询ToolStripMenuItem.Text = "班级查询";
            this.班级查询ToolStripMenuItem.Click += new System.EventHandler(this.班级查询ToolStripMenuItem_Click);
            // 
            // 删除班级ToolStripMenuItem
            // 
            this.删除班级ToolStripMenuItem.Name = "删除班级ToolStripMenuItem";
            this.删除班级ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.删除班级ToolStripMenuItem.Text = "删除班级";
            this.删除班级ToolStripMenuItem.Click += new System.EventHandler(this.删除班级ToolStripMenuItem_Click);
            // 
            // 课程管理ToolStripMenuItem
            // 
            this.课程管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.课程添加ToolStripMenuItem,
            this.课程查询ToolStripMenuItem,
            this.课程修改ToolStripMenuItem});
            this.课程管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.课程管理ToolStripMenuItem.Name = "课程管理ToolStripMenuItem";
            this.课程管理ToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.课程管理ToolStripMenuItem.Text = "课程管理";
            this.课程管理ToolStripMenuItem.Click += new System.EventHandler(this.课程管理ToolStripMenuItem_Click);
            // 
            // 课程添加ToolStripMenuItem
            // 
            this.课程添加ToolStripMenuItem.Name = "课程添加ToolStripMenuItem";
            this.课程添加ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.课程添加ToolStripMenuItem.Text = "课程添加";
            this.课程添加ToolStripMenuItem.Click += new System.EventHandler(this.课程添加ToolStripMenuItem_Click);
            // 
            // 课程查询ToolStripMenuItem
            // 
            this.课程查询ToolStripMenuItem.Name = "课程查询ToolStripMenuItem";
            this.课程查询ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.课程查询ToolStripMenuItem.Text = "课程查询";
            this.课程查询ToolStripMenuItem.Click += new System.EventHandler(this.课程查询ToolStripMenuItem_Click);
            // 
            // 课程修改ToolStripMenuItem
            // 
            this.课程修改ToolStripMenuItem.Name = "课程修改ToolStripMenuItem";
            this.课程修改ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.课程修改ToolStripMenuItem.Text = "课程修改";
            this.课程修改ToolStripMenuItem.Click += new System.EventHandler(this.课程修改ToolStripMenuItem_Click);
            // 
            // 成绩管理ToolStripMenuItem
            // 
            this.成绩管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.成绩添加ToolStripMenuItem,
            this.成绩查询ToolStripMenuItem,
            this.修改成绩ToolStripMenuItem,
            this.删除成绩ToolStripMenuItem});
            this.成绩管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.成绩管理ToolStripMenuItem.Name = "成绩管理ToolStripMenuItem";
            this.成绩管理ToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.成绩管理ToolStripMenuItem.Text = "成绩管理";
            // 
            // 成绩添加ToolStripMenuItem
            // 
            this.成绩添加ToolStripMenuItem.Name = "成绩添加ToolStripMenuItem";
            this.成绩添加ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.成绩添加ToolStripMenuItem.Text = "成绩添加";
            this.成绩添加ToolStripMenuItem.Click += new System.EventHandler(this.成绩添加ToolStripMenuItem_Click);
            // 
            // 成绩查询ToolStripMenuItem
            // 
            this.成绩查询ToolStripMenuItem.Name = "成绩查询ToolStripMenuItem";
            this.成绩查询ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.成绩查询ToolStripMenuItem.Text = "成绩查询";
            this.成绩查询ToolStripMenuItem.Click += new System.EventHandler(this.成绩查询ToolStripMenuItem_Click);
            // 
            // 修改成绩ToolStripMenuItem
            // 
            this.修改成绩ToolStripMenuItem.Name = "修改成绩ToolStripMenuItem";
            this.修改成绩ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.修改成绩ToolStripMenuItem.Text = "修改成绩";
            this.修改成绩ToolStripMenuItem.Click += new System.EventHandler(this.修改成绩ToolStripMenuItem_Click);
            // 
            // 删除成绩ToolStripMenuItem
            // 
            this.删除成绩ToolStripMenuItem.Name = "删除成绩ToolStripMenuItem";
            this.删除成绩ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.删除成绩ToolStripMenuItem.Text = "删除成绩";
            this.删除成绩ToolStripMenuItem.Click += new System.EventHandler(this.删除成绩ToolStripMenuItem_Click);
            // 
            // 缴费管理ToolStripMenuItem
            // 
            this.缴费管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.学生交费增加ToolStripMenuItem,
            this.学生交费查询ToolStripMenuItem,
            this.学生交费修改ToolStripMenuItem});
            this.缴费管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.缴费管理ToolStripMenuItem.Name = "缴费管理ToolStripMenuItem";
            this.缴费管理ToolStripMenuItem.Size = new System.Drawing.Size(81, 24);
            this.缴费管理ToolStripMenuItem.Text = "缴费管理";
            this.缴费管理ToolStripMenuItem.Click += new System.EventHandler(this.缴费管理ToolStripMenuItem_Click);
            // 
            // 学生交费增加ToolStripMenuItem
            // 
            this.学生交费增加ToolStripMenuItem.Name = "学生交费增加ToolStripMenuItem";
            this.学生交费增加ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.学生交费增加ToolStripMenuItem.Text = "学生交费增加";
            this.学生交费增加ToolStripMenuItem.Click += new System.EventHandler(this.学生交费增加ToolStripMenuItem_Click);
            // 
            // 学生交费查询ToolStripMenuItem
            // 
            this.学生交费查询ToolStripMenuItem.Name = "学生交费查询ToolStripMenuItem";
            this.学生交费查询ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.学生交费查询ToolStripMenuItem.Text = "交费记录查询";
            this.学生交费查询ToolStripMenuItem.Click += new System.EventHandler(this.交费记录查询ToolStripMenuItem_Click);
            // 
            // 学生交费修改ToolStripMenuItem
            // 
            this.学生交费修改ToolStripMenuItem.Name = "学生交费修改ToolStripMenuItem";
            this.学生交费修改ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.学生交费修改ToolStripMenuItem.Text = "学生交费修改";
            this.学生交费修改ToolStripMenuItem.Click += new System.EventHandler(this.学生交费修改ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.宿舍管理ToolStripMenuItem});
            this.toolStripMenuItem1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(81, 24);
            this.toolStripMenuItem1.Text = "宿舍管理";
            // 
            // 宿舍管理ToolStripMenuItem
            // 
            this.宿舍管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.宿舍管理ToolStripMenuItem.Name = "宿舍管理ToolStripMenuItem";
            this.宿舍管理ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.宿舍管理ToolStripMenuItem.Text = "宿舍管理";
            this.宿舍管理ToolStripMenuItem.Click += new System.EventHandler(this.宿舍管理ToolStripMenuItem_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.操作说明ToolStripMenuItem,
            this.关于ToolStripMenuItem,
            this.关于记事本ToolStripMenuItem});
            this.帮助ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(51, 24);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // 操作说明ToolStripMenuItem
            // 
            this.操作说明ToolStripMenuItem.Name = "操作说明ToolStripMenuItem";
            this.操作说明ToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.操作说明ToolStripMenuItem.Text = "操作说明";
            this.操作说明ToolStripMenuItem.Click += new System.EventHandler(this.操作说明ToolStripMenuItem_Click);
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.关于ToolStripMenuItem.Text = "关于";
            this.关于ToolStripMenuItem.Click += new System.EventHandler(this.关于ToolStripMenuItem_Click);
            // 
            // 关于记事本ToolStripMenuItem
            // 
            this.关于记事本ToolStripMenuItem.Name = "关于记事本ToolStripMenuItem";
            this.关于记事本ToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.关于记事本ToolStripMenuItem.Text = "关于记事本";
            this.关于记事本ToolStripMenuItem.Click += new System.EventHandler(this.关于记事本ToolStripMenuItem_Click_1);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.刷新ToolStripMenuItem,
            this.退出系统ToolStripMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(139, 52);
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(138, 24);
            this.刷新ToolStripMenuItem.Text = "刷新";
            // 
            // 退出系统ToolStripMenuItem1
            // 
            this.退出系统ToolStripMenuItem1.Name = "退出系统ToolStripMenuItem1";
            this.退出系统ToolStripMenuItem1.Size = new System.Drawing.Size(138, 24);
            this.退出系统ToolStripMenuItem1.Text = "退出系统";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 644);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 15);
            this.label1.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // 教师系统
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightCyan;
            this.ClientSize = new System.Drawing.Size(872, 661);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "教师系统";
            this.Text = "教师系统";
            this.Load += new System.EventHandler(this.老师_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 系统设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 增加用户ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 修改密码ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除用户ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 重新登录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出系统ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 班级管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 添加班级ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 班级查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除班级ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 学籍管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 档案增加ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 档案查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 注销学生管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 课程管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 课程添加ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 课程查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 课程修改ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 成绩管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 成绩添加ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 成绩查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 修改成绩ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 缴费管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 学生交费增加ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 学生交费查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 操作说明ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出系统ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 退出系统ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 查询用户ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 档案删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 删除成绩ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 学生交费修改ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 宿舍管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于记事本ToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;

    }
}