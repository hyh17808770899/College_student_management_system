﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;


namespace 学院学生管理系统
{
    public partial class 课程管理 : Form
    {
        public 课程管理()
        {
            InitializeComponent();
        }

        private void 课程查询_Load(object sender, EventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            string t;
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            if (textBox6.Text == "")
            {
                MessageBox.Show("请输入教师姓名！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 课程表 where 姓名='" + textBox6.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    textBox6.Text = a3[0].ToString();
                    textBox1.Text = a3[1].ToString();
                    textBox2.Text = a3[2].ToString();
                    textBox3.Text = a3[3].ToString();
                    textBox4.Text = a3[4].ToString();
                    textBox5.Text = a3[5].ToString();
                    MessageBox.Show("查询成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
                    OleDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "select * from 课程表";
                    conn.Open();
                    OleDbDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    if (dr.HasRows)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            dt.Columns.Add(dr.GetName(i));
                        }
                        dt.Rows.Clear();
                    }
                    while (dr.Read())
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            row[i] = dr[i];
                        }
                        dt.Rows.Add(row);
                    }
                    cmd.Dispose();
                    conn.Close();
                    dataGridView1.DataSource = dt;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string t;
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            t = "select * from 课程表 where 姓名='" + textBox6.Text.Trim() + "'";
            a2.CommandText = t;
            a2.Connection = a1;
            OleDbDataReader a3 = a2.ExecuteReader();
            if (a3.Read())
            {
                MessageBox.Show("信息已经存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            else
            {
                a3.Close();
                string y;
                y = "insert into 课程表 values(" + "'" + textBox6.Text.Trim() + "','" + textBox1.Text.Trim() + "','" + textBox2.Text.Trim() + "','" + textBox3.Text.Trim() + "','" + textBox4.Text.Trim() + "','" + textBox5.Text.Trim() + "')";
                if (textBox6.Text == "" || textBox1.Text == "" || textBox2.Text == "" || textBox3.Text == "" || textBox4.Text == "" || textBox5.Text == "")
                {
                    MessageBox.Show("信息不能为空！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    a2.CommandText = y;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("信息添加成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
            OleDbCommand cmd = conn.CreateCommand();
            cmd.CommandText = "select * from 课程表";
            conn.Open();
            OleDbDataReader dr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            if (dr.HasRows)
            {
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    dt.Columns.Add(dr.GetName(i));
                }
                dt.Rows.Clear();
            }
            while (dr.Read())
            {
                DataRow row = dt.NewRow();
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    row[i] = dr[i];
                }
                dt.Rows.Add(row);
            }
            cmd.Dispose();
            conn.Close();
            dataGridView1.DataSource = dt;
                }
            } 
        }

        private void button6_Click(object sender, EventArgs e)
        {
            string t;
            t = "select * from 课程表 where 姓名='" + textBox6.Text.Trim() + "'";
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            if (textBox6.Text == "")
            {
                MessageBox.Show("请输入需要修改的班级的班名！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 课程表 where 姓名='" + textBox6.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    a3.Close();
                    string aw;
                    aw = "update 课程表 set 周一='" + textBox1.Text.Trim() + "',周二='" + textBox2.Text.Trim() + "',周三='" + textBox3.Text.Trim() + "',周四='" + textBox4.Text.Trim() + "',周五='" + textBox5.Text.Trim() + "'where 姓名='" + textBox6.Text.Trim() + "'";
                    a2.CommandText = aw;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("修改信息成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
                    OleDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "select * from 课程表";
                    conn.Open();
                    OleDbDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    if (dr.HasRows)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            dt.Columns.Add(dr.GetName(i));
                        }
                        dt.Rows.Clear();
                    }
                    while (dr.Read())
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            row[i] = dr[i];
                        }
                        dt.Rows.Add(row);
                    }
                    cmd.Dispose();
                    conn.Close();
                    dataGridView1.DataSource = dt;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            string t;
            t = "select * from 课程表 where 姓名='" + textBox6.Text.Trim() + "'";
            OleDbConnection a1 = new OleDbConnection();
            OleDbCommand a2 = new OleDbCommand();
            a1.ConnectionString = Class1.mas;
            a1.Open();

            if (textBox6.Text == "")
            {
                MessageBox.Show("请输入需要删除的人的姓名！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                t = "select * from 课程表 where 姓名='" + textBox6.Text.Trim() + "'";
                a2.CommandText = t;
                a2.Connection = a1;
                OleDbDataReader a3 = a2.ExecuteReader();
                if (a3.Read() == false)
                {
                    MessageBox.Show("信息不存在！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    a3.Close();
                    string aw;
                    aw = "delete from 课程表 where 姓名='" + textBox6.Text.Trim() + "'";
                    a2.CommandText = aw;
                    a2.Connection = a1;
                    a2.ExecuteNonQuery();
                    MessageBox.Show("信息删除成功！", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    OleDbConnection conn = new OleDbConnection("Provider=Microsoft.ace.oledb.12.0;Data Source=登录系统.accdb"); //Jet OLEDB:Database Password=
                    OleDbCommand cmd = conn.CreateCommand();
                    cmd.CommandText = "select * from 课程表";
                    conn.Open();
                    OleDbDataReader dr = cmd.ExecuteReader();
                    DataTable dt = new DataTable();
                    if (dr.HasRows)
                    {
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            dt.Columns.Add(dr.GetName(i));
                        }
                        dt.Rows.Clear();
                    }
                    while (dr.Read())
                    {
                        DataRow row = dt.NewRow();
                        for (int i = 0; i < dr.FieldCount; i++)
                        {
                            row[i] = dr[i];
                        }
                        dt.Rows.Add(row);
                    }
                    cmd.Dispose();
                    conn.Close();
                    dataGridView1.DataSource = dt;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            教师系统 c2 = new 教师系统();
            c2.Show();
            this.Hide();
        }
    }
}
