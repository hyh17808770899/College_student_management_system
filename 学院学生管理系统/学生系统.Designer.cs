﻿namespace 学院学生管理系统
{
    partial class 学生系统
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.系统设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.用户管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.修改密码ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.重新登录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出系统ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出系统ToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.学籍管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.档案查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.班级管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.班级查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.课程管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.课程查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.成绩管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.成绩查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.缴费管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.学生交费增加ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.学生交费查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.学生交费修改ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.宿舍管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.故障报修ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.故障报修查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.充值与缴费ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.电费充值ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.水费充值ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.住宿费缴费ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.帮助ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.操作说明ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.系统设置ToolStripMenuItem,
            this.学籍管理ToolStripMenuItem,
            this.班级管理ToolStripMenuItem,
            this.课程管理ToolStripMenuItem,
            this.成绩管理ToolStripMenuItem,
            this.缴费管理ToolStripMenuItem,
            this.toolStripMenuItem1,
            this.帮助ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(631, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 系统设置ToolStripMenuItem
            // 
            this.系统设置ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.用户管理ToolStripMenuItem,
            this.重新登录ToolStripMenuItem,
            this.退出系统ToolStripMenuItem,
            this.退出系统ToolStripMenuItem2});
            this.系统设置ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.系统设置ToolStripMenuItem.Name = "系统设置ToolStripMenuItem";
            this.系统设置ToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.系统设置ToolStripMenuItem.Text = "系统设置";
            // 
            // 用户管理ToolStripMenuItem
            // 
            this.用户管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.修改密码ToolStripMenuItem});
            this.用户管理ToolStripMenuItem.Name = "用户管理ToolStripMenuItem";
            this.用户管理ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.用户管理ToolStripMenuItem.Text = "用户管理";
            // 
            // 修改密码ToolStripMenuItem
            // 
            this.修改密码ToolStripMenuItem.Name = "修改密码ToolStripMenuItem";
            this.修改密码ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.修改密码ToolStripMenuItem.Text = "修改密码";
            this.修改密码ToolStripMenuItem.Click += new System.EventHandler(this.修改密码ToolStripMenuItem_Click);
            // 
            // 重新登录ToolStripMenuItem
            // 
            this.重新登录ToolStripMenuItem.Name = "重新登录ToolStripMenuItem";
            this.重新登录ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.重新登录ToolStripMenuItem.Text = "记事本";
            this.重新登录ToolStripMenuItem.Click += new System.EventHandler(this.重新登录ToolStripMenuItem_Click);
            // 
            // 退出系统ToolStripMenuItem
            // 
            this.退出系统ToolStripMenuItem.Name = "退出系统ToolStripMenuItem";
            this.退出系统ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.退出系统ToolStripMenuItem.Text = "重新登录";
            this.退出系统ToolStripMenuItem.Click += new System.EventHandler(this.退出系统ToolStripMenuItem_Click);
            // 
            // 退出系统ToolStripMenuItem2
            // 
            this.退出系统ToolStripMenuItem2.Name = "退出系统ToolStripMenuItem2";
            this.退出系统ToolStripMenuItem2.Size = new System.Drawing.Size(118, 22);
            this.退出系统ToolStripMenuItem2.Text = "退出系统";
            this.退出系统ToolStripMenuItem2.Click += new System.EventHandler(this.退出系统ToolStripMenuItem2_Click);
            // 
            // 学籍管理ToolStripMenuItem
            // 
            this.学籍管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.档案查询ToolStripMenuItem});
            this.学籍管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.学籍管理ToolStripMenuItem.Name = "学籍管理ToolStripMenuItem";
            this.学籍管理ToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.学籍管理ToolStripMenuItem.Text = "学籍管理";
            // 
            // 档案查询ToolStripMenuItem
            // 
            this.档案查询ToolStripMenuItem.Name = "档案查询ToolStripMenuItem";
            this.档案查询ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.档案查询ToolStripMenuItem.Text = "档案查询";
            this.档案查询ToolStripMenuItem.Click += new System.EventHandler(this.档案查询ToolStripMenuItem_Click);
            // 
            // 班级管理ToolStripMenuItem
            // 
            this.班级管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.班级查询ToolStripMenuItem});
            this.班级管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.班级管理ToolStripMenuItem.Name = "班级管理ToolStripMenuItem";
            this.班级管理ToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.班级管理ToolStripMenuItem.Text = "班级管理";
            // 
            // 班级查询ToolStripMenuItem
            // 
            this.班级查询ToolStripMenuItem.Name = "班级查询ToolStripMenuItem";
            this.班级查询ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.班级查询ToolStripMenuItem.Text = "班级查询";
            this.班级查询ToolStripMenuItem.Click += new System.EventHandler(this.班级查询ToolStripMenuItem_Click);
            // 
            // 课程管理ToolStripMenuItem
            // 
            this.课程管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.课程查询ToolStripMenuItem});
            this.课程管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.课程管理ToolStripMenuItem.Name = "课程管理ToolStripMenuItem";
            this.课程管理ToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.课程管理ToolStripMenuItem.Text = "课程管理";
            // 
            // 课程查询ToolStripMenuItem
            // 
            this.课程查询ToolStripMenuItem.Name = "课程查询ToolStripMenuItem";
            this.课程查询ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.课程查询ToolStripMenuItem.Text = "课程查询";
            this.课程查询ToolStripMenuItem.Click += new System.EventHandler(this.课程查询ToolStripMenuItem_Click);
            // 
            // 成绩管理ToolStripMenuItem
            // 
            this.成绩管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.成绩查询ToolStripMenuItem});
            this.成绩管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.成绩管理ToolStripMenuItem.Name = "成绩管理ToolStripMenuItem";
            this.成绩管理ToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.成绩管理ToolStripMenuItem.Text = "成绩管理";
            // 
            // 成绩查询ToolStripMenuItem
            // 
            this.成绩查询ToolStripMenuItem.Name = "成绩查询ToolStripMenuItem";
            this.成绩查询ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.成绩查询ToolStripMenuItem.Text = "成绩查询";
            this.成绩查询ToolStripMenuItem.Click += new System.EventHandler(this.成绩查询ToolStripMenuItem_Click);
            // 
            // 缴费管理ToolStripMenuItem
            // 
            this.缴费管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.学生交费增加ToolStripMenuItem,
            this.学生交费查询ToolStripMenuItem,
            this.学生交费修改ToolStripMenuItem});
            this.缴费管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.缴费管理ToolStripMenuItem.Name = "缴费管理ToolStripMenuItem";
            this.缴费管理ToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.缴费管理ToolStripMenuItem.Text = "缴费管理";
            // 
            // 学生交费增加ToolStripMenuItem
            // 
            this.学生交费增加ToolStripMenuItem.Name = "学生交费增加ToolStripMenuItem";
            this.学生交费增加ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.学生交费增加ToolStripMenuItem.Text = "学生交费增加";
            this.学生交费增加ToolStripMenuItem.Click += new System.EventHandler(this.学生交费增加ToolStripMenuItem_Click);
            // 
            // 学生交费查询ToolStripMenuItem
            // 
            this.学生交费查询ToolStripMenuItem.Name = "学生交费查询ToolStripMenuItem";
            this.学生交费查询ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.学生交费查询ToolStripMenuItem.Text = "交费记录查询";
            this.学生交费查询ToolStripMenuItem.Click += new System.EventHandler(this.学生交费查询ToolStripMenuItem_Click);
            // 
            // 学生交费修改ToolStripMenuItem
            // 
            this.学生交费修改ToolStripMenuItem.Name = "学生交费修改ToolStripMenuItem";
            this.学生交费修改ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.学生交费修改ToolStripMenuItem.Text = "学生交费修改";
            this.学生交费修改ToolStripMenuItem.Click += new System.EventHandler(this.学生交费修改ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.宿舍管理ToolStripMenuItem,
            this.充值与缴费ToolStripMenuItem});
            this.toolStripMenuItem1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(65, 20);
            this.toolStripMenuItem1.Text = "宿舍管理";
            // 
            // 宿舍管理ToolStripMenuItem
            // 
            this.宿舍管理ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.故障报修ToolStripMenuItem,
            this.故障报修查询ToolStripMenuItem});
            this.宿舍管理ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.宿舍管理ToolStripMenuItem.Name = "宿舍管理ToolStripMenuItem";
            this.宿舍管理ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.宿舍管理ToolStripMenuItem.Text = "宿舍器材";
            this.宿舍管理ToolStripMenuItem.Click += new System.EventHandler(this.宿舍管理ToolStripMenuItem_Click);
            // 
            // 故障报修ToolStripMenuItem
            // 
            this.故障报修ToolStripMenuItem.Name = "故障报修ToolStripMenuItem";
            this.故障报修ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.故障报修ToolStripMenuItem.Text = "故障报修";
            this.故障报修ToolStripMenuItem.Click += new System.EventHandler(this.故障报修ToolStripMenuItem_Click);
            // 
            // 故障报修查询ToolStripMenuItem
            // 
            this.故障报修查询ToolStripMenuItem.Name = "故障报修查询ToolStripMenuItem";
            this.故障报修查询ToolStripMenuItem.Size = new System.Drawing.Size(142, 22);
            this.故障报修查询ToolStripMenuItem.Text = "故障报修查询";
            this.故障报修查询ToolStripMenuItem.Click += new System.EventHandler(this.故障报修查询ToolStripMenuItem_Click);
            // 
            // 充值与缴费ToolStripMenuItem
            // 
            this.充值与缴费ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.电费充值ToolStripMenuItem,
            this.水费充值ToolStripMenuItem,
            this.住宿费缴费ToolStripMenuItem});
            this.充值与缴费ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.充值与缴费ToolStripMenuItem.Name = "充值与缴费ToolStripMenuItem";
            this.充值与缴费ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.充值与缴费ToolStripMenuItem.Text = "充值与缴费";
            // 
            // 电费充值ToolStripMenuItem
            // 
            this.电费充值ToolStripMenuItem.Name = "电费充值ToolStripMenuItem";
            this.电费充值ToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.电费充值ToolStripMenuItem.Text = "电费充值";
            this.电费充值ToolStripMenuItem.Click += new System.EventHandler(this.电费充值ToolStripMenuItem_Click);
            // 
            // 水费充值ToolStripMenuItem
            // 
            this.水费充值ToolStripMenuItem.Name = "水费充值ToolStripMenuItem";
            this.水费充值ToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.水费充值ToolStripMenuItem.Text = "水费充值";
            this.水费充值ToolStripMenuItem.Click += new System.EventHandler(this.水费充值ToolStripMenuItem_Click);
            // 
            // 住宿费缴费ToolStripMenuItem
            // 
            this.住宿费缴费ToolStripMenuItem.Name = "住宿费缴费ToolStripMenuItem";
            this.住宿费缴费ToolStripMenuItem.Size = new System.Drawing.Size(130, 22);
            this.住宿费缴费ToolStripMenuItem.Text = "住宿费缴费";
            this.住宿费缴费ToolStripMenuItem.Click += new System.EventHandler(this.住宿费缴费ToolStripMenuItem_Click);
            // 
            // 帮助ToolStripMenuItem
            // 
            this.帮助ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.操作说明ToolStripMenuItem,
            this.关于ToolStripMenuItem});
            this.帮助ToolStripMenuItem.ForeColor = System.Drawing.SystemColors.Highlight;
            this.帮助ToolStripMenuItem.Name = "帮助ToolStripMenuItem";
            this.帮助ToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.帮助ToolStripMenuItem.Text = "帮助";
            // 
            // 操作说明ToolStripMenuItem
            // 
            this.操作说明ToolStripMenuItem.Name = "操作说明ToolStripMenuItem";
            this.操作说明ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.操作说明ToolStripMenuItem.Text = "操作说明";
            this.操作说明ToolStripMenuItem.Click += new System.EventHandler(this.操作说明ToolStripMenuItem_Click);
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(118, 22);
            this.关于ToolStripMenuItem.Text = "关于";
            this.关于ToolStripMenuItem.Click += new System.EventHandler(this.关于ToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 543);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 12);
            this.label1.TabIndex = 2;
            // 
            // 学生系统
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(631, 562);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "学生系统";
            this.Text = "学生系统";
            this.Load += new System.EventHandler(this.学生系统_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 系统设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 用户管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 修改密码ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 重新登录ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出系统ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出系统ToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem 学籍管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 档案查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 班级管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 班级查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 课程管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 课程查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 成绩管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 成绩查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 缴费管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 学生交费增加ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 学生交费查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 学生交费修改ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 宿舍管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 帮助ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 操作说明ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 充值与缴费ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 故障报修ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 故障报修查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 电费充值ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 水费充值ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 住宿费缴费ToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label1;
    }
}