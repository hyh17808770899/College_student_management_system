﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace 学院学生管理系统
{
    public partial class 教师系统 : Form
    {
        public 教师系统()
        {
            InitializeComponent();
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void 档案修改ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学籍管理 a10 = new 学籍管理();
            a10.Show();
        }

        private void 增加用户ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            添加用户 a1 = new 添加用户();
            a1.Show();
        }

        private void 退出系统ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            登录页面 c1 = new 登录页面();
            c1.Show();
        }

        private void 添加班级ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            班级管理 a4 = new 班级管理();
            a4.Show();
        }

        private void 老师_Load(object sender, EventArgs e)
        {
            label1.Text = "时间：" + DateTime.Now.ToLongTimeString().ToString();
            timer1.Enabled = true;
            timer1.Interval = 100;
        }

        private void 缴费管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 用户管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 修改密码ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            修改密码 a2 = new 修改密码();
            a2.Show();
        }

        private void 删除用户ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            查询或删除用户 a3 = new 查询或删除用户();
            a3.Show();
        }

        private void 班级查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            班级管理 a5 = new 班级管理();
            a5.Show();
        }

        private void 删除班级ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            班级管理 a6 = new 班级管理();
            a6.Show();
        }

        private void 档案增加ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学籍管理 a7 = new 学籍管理();
            a7.Show();
        }

        private void 档案查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学籍管理 a8 = new 学籍管理();
            a8.Show();
        }

        private void 注销学生管理ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            学籍管理 a9 = new 学籍管理 ();
            a9.Show();
        }

        private void 课程添加ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            课程管理 a11 = new 课程管理();
            a11.Show();
        }

        private void 课程查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            课程管理 a12 = new 课程管理();
            a12.Show();
        }

        private void 课程修改ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            课程管理 a13 = new 课程管理();
            a13.Show();
        }

        private void 成绩添加ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            成绩管理 a14 = new 成绩管理();
            a14.Show();
        }

        private void 成绩查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            成绩管理 a15 = new 成绩管理();
            a15.Show();
        }

        private void 修改成绩ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            成绩管理 a16 = new 成绩管理();
            a16.Show();
        }

        private void 学生交费增加ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学生交费 a17 = new 学生交费();
            a17.Show();
        }

        private void 交费记录查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学生交费 a18 = new 学生交费();
            a18.Show();
        }

        private void 操作说明ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            操作说明 a19 = new 操作说明();
            a19.Show();
        }

        private void 关于ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            关于 a20 = new 关于();
            a20.Show();
        }

        private void 查询用户ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            查询或删除用户 a21 = new 查询或删除用户();
            a21.Show();
        }

        private void 退出系统ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("您确定退出吗?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void 档案删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学籍管理 a22 = new 学籍管理();
            a22.Show();
        }

        private void 课程管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 删除成绩ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            成绩管理 a23 = new 成绩管理();
            a23.Show();
        }

        private void 宿舍故障报修ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 宿舍管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
             宿舍管理系统 a20 = new 宿舍管理系统();
            a20.Show();
        }

        private void 学生交费修改ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学生交费 a21 = new 学生交费();
            a21.Show();
        }

        private void 重新登录ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            记事本 a22 = new 记事本();
            a22.Show();
        }

        private void 关于记事本ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void 关于记事本ToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            关于记事本 q1 = new 关于记事本();
            q1.Show();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = "当前时间为：" + DateTime.Now.ToLongTimeString().ToString();
        }
    }
}
