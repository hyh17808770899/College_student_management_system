﻿namespace WindowsFormsApplication1
{
    partial class 宿舍管理系统
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(宿舍管理系统));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.管理人员ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.出入登记ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.人员来访登记ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.人员来访登记查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.学生ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.故障报修ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.故障保修查询ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.充值与缴费ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.电费充值ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.水费充值ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.住宿费缴费ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("menuStrip1.BackgroundImage")));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.管理人员ToolStripMenuItem,
            this.学生ToolStripMenuItem,
            this.充值与缴费ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(675, 27);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 管理人员ToolStripMenuItem
            // 
            this.管理人员ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.出入登记ToolStripMenuItem,
            this.人员来访登记ToolStripMenuItem,
            this.toolStripMenuItem1,
            this.人员来访登记查询ToolStripMenuItem});
            this.管理人员ToolStripMenuItem.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.管理人员ToolStripMenuItem.Name = "管理人员ToolStripMenuItem";
            this.管理人员ToolStripMenuItem.Size = new System.Drawing.Size(81, 23);
            this.管理人员ToolStripMenuItem.Text = "管理人员";
            // 
            // 出入登记ToolStripMenuItem
            // 
            this.出入登记ToolStripMenuItem.Name = "出入登记ToolStripMenuItem";
            this.出入登记ToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.出入登记ToolStripMenuItem.Text = "出入登记";
            this.出入登记ToolStripMenuItem.Click += new System.EventHandler(this.出入登记ToolStripMenuItem_Click);
            // 
            // 人员来访登记ToolStripMenuItem
            // 
            this.人员来访登记ToolStripMenuItem.Name = "人员来访登记ToolStripMenuItem";
            this.人员来访登记ToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.人员来访登记ToolStripMenuItem.Text = "人员来访登记";
            this.人员来访登记ToolStripMenuItem.Click += new System.EventHandler(this.人员来访登记ToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(198, 24);
            this.toolStripMenuItem1.Text = "出入登记查询";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // 人员来访登记查询ToolStripMenuItem
            // 
            this.人员来访登记查询ToolStripMenuItem.Name = "人员来访登记查询ToolStripMenuItem";
            this.人员来访登记查询ToolStripMenuItem.Size = new System.Drawing.Size(198, 24);
            this.人员来访登记查询ToolStripMenuItem.Text = "人员来访登记查询";
            this.人员来访登记查询ToolStripMenuItem.Click += new System.EventHandler(this.人员来访登记查询ToolStripMenuItem_Click);
            // 
            // 学生ToolStripMenuItem
            // 
            this.学生ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.故障报修ToolStripMenuItem,
            this.故障保修查询ToolStripMenuItem});
            this.学生ToolStripMenuItem.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.学生ToolStripMenuItem.Name = "学生ToolStripMenuItem";
            this.学生ToolStripMenuItem.Size = new System.Drawing.Size(51, 23);
            this.学生ToolStripMenuItem.Text = "学生";
            // 
            // 故障报修ToolStripMenuItem
            // 
            this.故障报修ToolStripMenuItem.Name = "故障报修ToolStripMenuItem";
            this.故障报修ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.故障报修ToolStripMenuItem.Text = "故障报修";
            this.故障报修ToolStripMenuItem.Click += new System.EventHandler(this.故障报修ToolStripMenuItem_Click);
            // 
            // 故障保修查询ToolStripMenuItem
            // 
            this.故障保修查询ToolStripMenuItem.Name = "故障保修查询ToolStripMenuItem";
            this.故障保修查询ToolStripMenuItem.Size = new System.Drawing.Size(168, 24);
            this.故障保修查询ToolStripMenuItem.Text = "故障报修查询";
            this.故障保修查询ToolStripMenuItem.Click += new System.EventHandler(this.故障保修查询ToolStripMenuItem_Click);
            // 
            // 充值与缴费ToolStripMenuItem
            // 
            this.充值与缴费ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.电费充值ToolStripMenuItem,
            this.水费充值ToolStripMenuItem,
            this.住宿费缴费ToolStripMenuItem});
            this.充值与缴费ToolStripMenuItem.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.充值与缴费ToolStripMenuItem.Name = "充值与缴费ToolStripMenuItem";
            this.充值与缴费ToolStripMenuItem.Size = new System.Drawing.Size(96, 23);
            this.充值与缴费ToolStripMenuItem.Text = "充值与缴费";
            // 
            // 电费充值ToolStripMenuItem
            // 
            this.电费充值ToolStripMenuItem.Name = "电费充值ToolStripMenuItem";
            this.电费充值ToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.电费充值ToolStripMenuItem.Text = "电费充值";
            this.电费充值ToolStripMenuItem.Click += new System.EventHandler(this.电费充值ToolStripMenuItem_Click);
            // 
            // 水费充值ToolStripMenuItem
            // 
            this.水费充值ToolStripMenuItem.Name = "水费充值ToolStripMenuItem";
            this.水费充值ToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.水费充值ToolStripMenuItem.Text = "水费充值";
            this.水费充值ToolStripMenuItem.Click += new System.EventHandler(this.水费充值ToolStripMenuItem_Click);
            // 
            // 住宿费缴费ToolStripMenuItem
            // 
            this.住宿费缴费ToolStripMenuItem.Name = "住宿费缴费ToolStripMenuItem";
            this.住宿费缴费ToolStripMenuItem.Size = new System.Drawing.Size(153, 24);
            this.住宿费缴费ToolStripMenuItem.Text = "住宿费缴费";
            this.住宿费缴费ToolStripMenuItem.Click += new System.EventHandler(this.住宿费缴费ToolStripMenuItem_Click);
            // 
            // 宿舍管理系统
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(675, 446);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "宿舍管理系统";
            this.Text = "宿舍管理";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 管理人员ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 出入登记ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 人员来访登记ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 学生ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 故障报修ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 充值与缴费ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 电费充值ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 水费充值ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 住宿费缴费ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem 人员来访登记查询ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 故障保修查询ToolStripMenuItem;
    }
}

