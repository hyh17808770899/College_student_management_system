﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WindowsFormsApplication1;

namespace 学院学生管理系统
{
    public partial class 学生系统 : Form
    {
        public 学生系统()
        {
            InitializeComponent();
        }

        private void 关于ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            关于 a20 = new 关于();
            a20.Show();
        }

        private void 操作说明ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            操作说明 a19 = new 操作说明();
            a19.Show();
        }

        private void 宿舍管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void 成绩添加ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 修改密码ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            修改密码 a20=new 修改密码();
            a20.Show();
        }

        private void 重新登录ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            记事本 a33 = new 记事本();
            a33.Show();
        }

        private void 退出系统ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            登录页面 c1 = new 登录页面();
            c1.Show();
        }

        private void 退出系统ToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("您确定退出吗?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void 档案查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学生学籍 s2 = new 学生学籍();
            s2.Show();
        }

        private void 班级查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学生班级 s3 = new 学生班级();
            s3.Show();
        }

        private void 成绩查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            成绩查询 s4 = new 成绩查询();
            s4.Show();
        }

        private void 修改成绩ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void 学生交费增加ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学生交费 s5 = new 学生交费();
            s5.Show();
        }

        private void 故障报修ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            宿舍故障报修 s7 = new 宿舍故障报修();
            s7.Show();
        }

        private void 故障报修查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            宿舍故障报修查询 s8 = new 宿舍故障报修查询();
            s8.Show();
        }

        private void 电费充值ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            电费充值 s9 = new 电费充值();
            s9.Show();
        }

        private void 水费充值ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            水费充值 S10 = new 水费充值();
            S10.Show();
        }

        private void 住宿费缴费ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            住宿费缴费 s11 = new 住宿费缴费();
            s11.Show();
        }

        private void 课程查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            课程查询 z1 = new 课程查询();
            z1.Show();
        }

        private void 学生系统_Load(object sender, EventArgs e)
        {
            label1.Text = "时间：" + DateTime.Now.ToLongTimeString().ToString();
            timer1.Enabled = true;
            timer1.Interval = 100;
        }

        private void 学生交费查询ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学生交费 s5 = new 学生交费();
            s5.Show();
        }

        private void 学生交费修改ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            学生交费 s5 = new 学生交费();
            s5.Show();
        }
    }
}
