﻿namespace 学院学生管理系统
{
    partial class 学生学籍
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnQuert = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.minzu = new System.Windows.Forms.ComboBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.dtpBirth = new System.Windows.Forms.DateTimePicker();
            this.tbMianMao = new System.Windows.Forms.TextBox();
            this.tbXingBie = new System.Windows.Forms.TextBox();
            this.beizhu = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.phone = new System.Windows.Forms.TextBox();
            this.postcodes = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.zhuzhi = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.XiBie = new System.Windows.Forms.TextBox();
            this.name = new System.Windows.Forms.TextBox();
            this.BanJi = new System.Windows.Forms.TextBox();
            this.no = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnSelect = new System.Windows.Forms.Button();
            this.tbGJZ = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbTiaoJian = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnXiuGai = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(39, 578);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowTemplate.Height = 23;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(917, 240);
            this.dataGridView1.TabIndex = 78;
            // 
            // btnQuert
            // 
            this.btnQuert.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnQuert.Location = new System.Drawing.Point(856, 843);
            this.btnQuert.Margin = new System.Windows.Forms.Padding(4);
            this.btnQuert.Name = "btnQuert";
            this.btnQuert.Size = new System.Drawing.Size(100, 29);
            this.btnQuert.TabIndex = 76;
            this.btnQuert.Text = "退出";
            this.btnQuert.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.minzu);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label17);
            this.groupBox1.Controls.Add(this.dateTimePicker1);
            this.groupBox1.Controls.Add(this.dtpBirth);
            this.groupBox1.Controls.Add(this.tbMianMao);
            this.groupBox1.Controls.Add(this.tbXingBie);
            this.groupBox1.Controls.Add(this.beizhu);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.phone);
            this.groupBox1.Controls.Add(this.postcodes);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.zhuzhi);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.XiBie);
            this.groupBox1.Controls.Add(this.name);
            this.groupBox1.Controls.Add(this.BanJi);
            this.groupBox1.Controls.Add(this.no);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label16);
            this.groupBox1.Location = new System.Drawing.Point(64, 93);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(819, 454);
            this.groupBox1.TabIndex = 74;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询结果";
            // 
            // minzu
            // 
            this.minzu.FormattingEnabled = true;
            this.minzu.Items.AddRange(new object[] {
            "汉族",
            "蒙古族",
            "满族",
            "朝鲜族",
            "赫哲族",
            "达斡尔族",
            "鄂温克族",
            "鄂伦春族",
            "回族",
            "东乡族",
            "土族",
            "撒拉族",
            "保安族",
            "裕固族",
            "维吾尔族",
            "哈萨克族",
            "柯尔克孜族",
            "锡伯族",
            "塔吉克族",
            "乌孜别克族",
            "俄罗斯族",
            "塔塔尔族",
            "藏族",
            "门巴族",
            "珞巴族",
            "羌族",
            "彝族",
            "白族",
            "哈尼族",
            "傣族",
            "僳僳族",
            "佤族",
            "拉祜族",
            "纳西族",
            "景颇族",
            "布朗族",
            "阿昌族",
            "普米族",
            "怒族",
            "德昂族",
            "独龙族",
            "基诺族",
            "苗族",
            "布依族",
            "侗族",
            "水族",
            "仡佬族",
            "壮族",
            "瑶族",
            "仫佬族",
            "毛南族",
            "京族",
            "土家族",
            "黎族",
            "畲族",
            "高山族"});
            this.minzu.Location = new System.Drawing.Point(93, 176);
            this.minzu.Margin = new System.Windows.Forms.Padding(4);
            this.minzu.Name = "minzu";
            this.minzu.Size = new System.Drawing.Size(133, 23);
            this.minzu.TabIndex = 76;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(632, 36);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 151);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 70;
            this.pictureBox1.TabStop = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.button2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button2.Location = new System.Drawing.Point(645, 192);
            this.button2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(99, 31);
            this.button2.TabIndex = 71;
            this.button2.Text = "选择照片";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(93, 228);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(320, 25);
            this.textBox1.TabIndex = 75;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 231);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(67, 15);
            this.label17.TabIndex = 74;
            this.label17.Text = "身份证号";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(657, 315);
            this.dateTimePicker1.Margin = new System.Windows.Forms.Padding(4);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(132, 25);
            this.dateTimePicker1.TabIndex = 73;
            // 
            // dtpBirth
            // 
            this.dtpBirth.Location = new System.Drawing.Point(377, 129);
            this.dtpBirth.Margin = new System.Windows.Forms.Padding(4);
            this.dtpBirth.Name = "dtpBirth";
            this.dtpBirth.Size = new System.Drawing.Size(132, 25);
            this.dtpBirth.TabIndex = 72;
            // 
            // tbMianMao
            // 
            this.tbMianMao.Location = new System.Drawing.Point(377, 176);
            this.tbMianMao.Margin = new System.Windows.Forms.Padding(4);
            this.tbMianMao.Name = "tbMianMao";
            this.tbMianMao.Size = new System.Drawing.Size(132, 25);
            this.tbMianMao.TabIndex = 71;
            // 
            // tbXingBie
            // 
            this.tbXingBie.Location = new System.Drawing.Point(377, 74);
            this.tbXingBie.Margin = new System.Windows.Forms.Padding(4);
            this.tbXingBie.Name = "tbXingBie";
            this.tbXingBie.Size = new System.Drawing.Size(132, 25);
            this.tbXingBie.TabIndex = 70;
            // 
            // beizhu
            // 
            this.beizhu.Location = new System.Drawing.Point(93, 380);
            this.beizhu.Margin = new System.Windows.Forms.Padding(4);
            this.beizhu.Multiline = true;
            this.beizhu.Name = "beizhu";
            this.beizhu.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.beizhu.Size = new System.Drawing.Size(700, 59);
            this.beizhu.TabIndex = 64;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 384);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 15);
            this.label14.TabIndex = 63;
            this.label14.Text = "备注";
            // 
            // phone
            // 
            this.phone.Location = new System.Drawing.Point(380, 315);
            this.phone.Margin = new System.Windows.Forms.Padding(4);
            this.phone.Name = "phone";
            this.phone.Size = new System.Drawing.Size(132, 25);
            this.phone.TabIndex = 62;
            // 
            // postcodes
            // 
            this.postcodes.Location = new System.Drawing.Point(93, 315);
            this.postcodes.Margin = new System.Windows.Forms.Padding(4);
            this.postcodes.Name = "postcodes";
            this.postcodes.Size = new System.Drawing.Size(132, 25);
            this.postcodes.TabIndex = 61;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(583, 320);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 15);
            this.label13.TabIndex = 60;
            this.label13.Text = "入学日期";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(301, 320);
            this.label12.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 15);
            this.label12.TabIndex = 59;
            this.label12.Text = "联系电话";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 320);
            this.label11.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 15);
            this.label11.TabIndex = 58;
            this.label11.Text = "邮政编码";
            // 
            // zhuzhi
            // 
            this.zhuzhi.Location = new System.Drawing.Point(93, 265);
            this.zhuzhi.Margin = new System.Windows.Forms.Padding(4);
            this.zhuzhi.Name = "zhuzhi";
            this.zhuzhi.Size = new System.Drawing.Size(700, 25);
            this.zhuzhi.TabIndex = 57;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 269);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 15);
            this.label10.TabIndex = 56;
            this.label10.Text = "家庭住址";
            // 
            // XiBie
            // 
            this.XiBie.Location = new System.Drawing.Point(95, 74);
            this.XiBie.Margin = new System.Windows.Forms.Padding(4);
            this.XiBie.Name = "XiBie";
            this.XiBie.Size = new System.Drawing.Size(132, 25);
            this.XiBie.TabIndex = 53;
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(377, 25);
            this.name.Margin = new System.Windows.Forms.Padding(4);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(132, 25);
            this.name.TabIndex = 52;
            // 
            // BanJi
            // 
            this.BanJi.AcceptsReturn = true;
            this.BanJi.Location = new System.Drawing.Point(95, 129);
            this.BanJi.Margin = new System.Windows.Forms.Padding(4);
            this.BanJi.Name = "BanJi";
            this.BanJi.Size = new System.Drawing.Size(132, 25);
            this.BanJi.TabIndex = 50;
            // 
            // no
            // 
            this.no.Location = new System.Drawing.Point(95, 25);
            this.no.Margin = new System.Windows.Forms.Padding(4);
            this.no.Name = "no";
            this.no.Size = new System.Drawing.Size(132, 25);
            this.no.TabIndex = 49;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(299, 180);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 15);
            this.label9.TabIndex = 48;
            this.label9.Text = "政治面貌";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(303, 132);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 15);
            this.label8.TabIndex = 47;
            this.label8.Text = "出生日期";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(32, 78);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 15);
            this.label7.TabIndex = 46;
            this.label7.Text = "系别";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(315, 78);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 15);
            this.label5.TabIndex = 45;
            this.label5.Text = "性别";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(315, 29);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 15);
            this.label4.TabIndex = 44;
            this.label4.Text = "姓名";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(32, 180);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 15);
            this.label3.TabIndex = 43;
            this.label3.Text = "民族";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(32, 132);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 15);
            this.label6.TabIndex = 42;
            this.label6.Text = "班级";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(32, 29);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(37, 15);
            this.label15.TabIndex = 41;
            this.label15.Text = "学号";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("宋体", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label16.ForeColor = System.Drawing.Color.Yellow;
            this.label16.Location = new System.Drawing.Point(576, 32);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 18);
            this.label16.TabIndex = 69;
            this.label16.Text = "照片：";
            // 
            // btnSelect
            // 
            this.btnSelect.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnSelect.Location = new System.Drawing.Point(835, 36);
            this.btnSelect.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(100, 29);
            this.btnSelect.TabIndex = 73;
            this.btnSelect.Text = "查询";
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // tbGJZ
            // 
            this.tbGJZ.Location = new System.Drawing.Point(517, 39);
            this.tbGJZ.Margin = new System.Windows.Forms.Padding(4);
            this.tbGJZ.Name = "tbGJZ";
            this.tbGJZ.Size = new System.Drawing.Size(276, 25);
            this.tbGJZ.TabIndex = 72;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(439, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 71;
            this.label2.Text = "关键字：";
            // 
            // cbTiaoJian
            // 
            this.cbTiaoJian.FormattingEnabled = true;
            this.cbTiaoJian.Items.AddRange(new object[] {
            "学号",
            "姓名",
            "班级",
            "系别"});
            this.cbTiaoJian.Location = new System.Drawing.Point(163, 40);
            this.cbTiaoJian.Margin = new System.Windows.Forms.Padding(4);
            this.cbTiaoJian.Name = "cbTiaoJian";
            this.cbTiaoJian.Size = new System.Drawing.Size(236, 23);
            this.cbTiaoJian.TabIndex = 70;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(68, 44);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 15);
            this.label1.TabIndex = 69;
            this.label1.Text = "查询条件：";
            // 
            // btnXiuGai
            // 
            this.btnXiuGai.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.btnXiuGai.Location = new System.Drawing.Point(721, 843);
            this.btnXiuGai.Margin = new System.Windows.Forms.Padding(4);
            this.btnXiuGai.Name = "btnXiuGai";
            this.btnXiuGai.Size = new System.Drawing.Size(100, 29);
            this.btnXiuGai.TabIndex = 79;
            this.btnXiuGai.Text = "修改";
            this.btnXiuGai.UseVisualStyleBackColor = true;
            this.btnXiuGai.Click += new System.EventHandler(this.btnXiuGai_Click);
            // 
            // 学生学籍
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSteelBlue;
            this.ClientSize = new System.Drawing.Size(1007, 891);
            this.Controls.Add(this.btnXiuGai);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btnQuert);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnSelect);
            this.Controls.Add(this.tbGJZ);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbTiaoJian);
            this.Controls.Add(this.label1);
            this.Name = "学生学籍";
            this.Text = "学生学籍";
            this.Load += new System.EventHandler(this.学生学籍_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnQuert;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox minzu;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.DateTimePicker dtpBirth;
        private System.Windows.Forms.TextBox tbMianMao;
        private System.Windows.Forms.TextBox tbXingBie;
        private System.Windows.Forms.TextBox beizhu;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox phone;
        private System.Windows.Forms.TextBox postcodes;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox zhuzhi;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox XiBie;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox BanJi;
        private System.Windows.Forms.TextBox no;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnSelect;
        private System.Windows.Forms.TextBox tbGJZ;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbTiaoJian;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnXiuGai;
    }
}